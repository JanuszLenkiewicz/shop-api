const express = require('express');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const router = express.Router();

const User = require('../models/user');

router.post('/signup', (req, res, next) => {
  bcrypt.hash(req.body.password, 10)
    .then(hash => {
      const user = new User({
        username: req.body.username,
        password: hash,
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        email: req.body.email,
        organization: req.body.organization,
        courses: req.body.courses,
        rodo: req.body.rodo
      });

      user.save()
        .then(result => {
          res.status(201).json({
            message: 'User created!',
            result: result
          });
        })
        .catch(err => {
          res.status(500).json({
            error: err
          })
        });
    });

});

router.post("/userlog", (req, res, next) => {
  let fetchedUser;

  User.findOne({username: req.body.username})
    .then(user => {
      if (!user) {
        return res.status(401).json({
          message: "Bed login or password."
        })
      }
      fetchedUser = user;
      return bcrypt.compare(req.body.password, user.password)
    })
    .then(result => {
      if (!result) {
        return res.status(401).json({
          message: 'Bed login or password.'
        });
      }
      const token = jwt.sign(
        { username: fetchedUser.username, userId: fetchedUser._id },
        process.env.JWT_KEY,
        {expiresIn: '1h'}
      );
      res.status(200).json({
        token: token,
        userId: fetchedUser._id,
        role: fetchedUser.role,
        card: fetchedUser.cart,
        isAuth: true
      })
    })
    .catch(err => {
      return res.status(401).json({
        message: 'Error of autorization.',
        error: err
      });
    });
});

router.get('', (req, res) => {
  User.find()
    .then(doc => {
      res.status(200).json({
        message: 'Users fetched successfully!',
        value: doc
      });
    })
    .catch(err => {
      res.status(400).json({
        message: 'Users can not be fetched',
        error: err
      });
    });
});

router.get('/courses/:id', (req, res) => {
  User.findById(req.params.id)
    .populate('courses.name', 'typeOfCourse typeOfContent countSlides countPoints shortname fullname link pass slides')
    .select('_id role rodo courses')
    .then(doc => {
      res.status(200).json({
        message: 'Users fetched successfully!',
        value: doc
      });
    })
    .catch(err => {
      res.status(400).json({
        message: 'Users can not be fetched',
        error: err
      });
    });
});

router.put('/edit/:user', (req, res)  => {
  User
    .findOne({_id: req.params.user})
    .then(user => {
      bcrypt.hash(req.body.password, 10)
        .then(hash => {
          user.password = hash;
        });
      user.username = req.body.username;
      user.firstname = req.body.firstname;
      user.lastname = req.body.lastname;
      user.email = req.body.email;
      user.organization = req.body.organization;
      user.save()
        .then(result => {
          res.status(201).json({
            message: 'User edited!',
            result: result
          });
        })
        .catch(err => {
          res.status(500).json({
            error: err
          })
        });
    });
});

router.put('/edit/:user', (req, res)  => {
  User
    .findOne({_id: req.params.user})
    .then(user => {
      bcrypt.hash(req.body.password, 10)
        .then(hash => {
          user.password = hash;
        });
      user.username = req.body.username;
      user.firstname = req.body.firstname;
      user.lastname = req.body.lastname;
      user.email = req.body.email;
      user.organization = req.body.organization;
      user.save()
        .then(result => {
          res.status(201).json({
            message: 'User edited!',
            result: result
          });
        })
        .catch(err => {
          res.status(500).json({
            error: err
          })
        });
    });
});

router.get('/profile/:id', (req, res) => {
  User
    .findById(req.params.id)
    .select('_id username firstname lastname email organization')
    .then(user => {
      if(user) {
        res.status(200).json({
          message: 'User profile has been downloaded!',
          data: user
        })
      } else {
        res.status(404).json({
          message: 'User not found!'
        })
      }
    });
});

module.exports = router;
