const express = require('express');

const Product = require('../models/product');
const checkAuth = require('../middleware/check-auth');

const router = express.Router();

router.post('', (req, res) => {
  const course = new Product(req.body);

  course.save()
    .then(createdProduct => {
      res.status(201).json({
        message: 'Product added successfully',
        data: createdProduct
      });
    })
    .catch(err => {
      res.json({success: false, err});
    });
});

router.get('', (req, res) => {
  Product.find()
    .then(doc => {
      res.status(200).json({
        message: 'Products fetched successfully!',
        value: doc
      });
    })
    .catch(err => {
      res.status(400).json({
        message: 'Users can not be fetched',
        error: err
      });
    });
});



module.exports = router;
