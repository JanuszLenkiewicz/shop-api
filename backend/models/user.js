const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

// role 0- user, 1 - admin
const userSchema = new mongoose.Schema({
  username: {type: String, required: true, unique:true},
  password: {type: String, required: true, minlength: 6},
  firstname: String,
  lastname: String,
  email: String,
  organization: String,
  cart:{type:Array,default: []},
  createdOn: {type: Date, default: Date.now},
  modifiedOn: Date,
  lastLogin: Date,
  role: {type:String, default:0},
  rodo: {type: Boolean, default: false}
});

userSchema.plugin(uniqueValidator);

module.exports = mongoose.model('User', userSchema);
