const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

const productSchema = new mongoose.Schema({
  name:{
    required: true,
    type: String,
    unique: 1,
    maxlength:100
  },
  count:{
    type: Number,
    maxlength: 10,
    default: 0
  },
  description:{
    required: true,
    type: String,
    maxlength:100000
  },
  price:{
    required: true,
    type: Number
  },
  brand:{
    required: true,
    type: String,
    maxlength:100000
  },
  shipping:{
    required: true,
    type: Boolean
  },
  available:{
    required: true,
    type: Boolean
  },
  publish:{
    required: true,
    type: Boolean,
    default:true
  },
  images:{
    type: Array,
    default:[]
  }
},{timestamps:true});

productSchema.plugin(uniqueValidator);
module.exports = mongoose.model('Product', productSchema);
